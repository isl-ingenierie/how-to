===================================
Comment utiliser GitLab.com à ISL ?
===================================

Ce guide a pour objectif d'aiguiller les ISLiens dans la configuration de leur compte GitLab.com, l'utilisation de ``git`` et `gitlab.com/isl-ingenierie <https://gitlab.com/isl-ingenierie>`_

Le GitLab (à l'exception de ce guide) n'est accessible que pour les membres enregistrés d'ISL. Un compte est nécessaire pour y accéder.

.. toctree::
    :maxdepth: 2

    guide/create_account
    guide/github_desktop
    guide/use_gitlab
