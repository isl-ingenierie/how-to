# GitHub Desktop

GitHub Desktop est une interface graphique pour `git` et permet de faciliter les commandes pour pousser des modifications.

   <p align="center">
   <img width="100%" src="../_static/github_desktop/gh_desktop_interface.png"/>
   </p>

Il peut être téléchargé [ici](https://desktop.github.com/).

## Configuration de base
