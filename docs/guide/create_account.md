# Créer son compte GitLab.com

Plusieurs méthodes s'offrent à vous :

- Sur invitation d'un membre ISL,
- En créant votre compte par vous-même,
- En utilisant votre compte GitHub, Google, ...

## Sur invitation d'un membre ISL

Si un membre ISL vous a invité à rejoindre GitLab.com, il suffit de cliquer sur le lien `Join now` dans l'email et renseigner les informations demandées.

   <p align="center">
   <img width="75%" src="../_static/create_account/mail_invit_1.png"/>
   </p>

Votre addresse ISL sera utilisée. Si vous souhaitez utiliser votre adresse personnelle, il est préférable que vous créiez votre compte par vous même en suivant les étapes du chapitre suivant.

## En créant votre compte par vous-même

Il suffit de se rendre à l'adresse suivante : <https://gitlab.com/users/sign_up>

   <p align="center">
   <img width="50%" src="../_static/create_account/sign_up.png"/>
   </p>

Le nom d'utilisateur sera à communiquer à Nicolas Godet ou Nicolas Van Hecke afin que vous soyez ajouté au groupe ISL.

## En utilisant votre compte GitHub, Google, ...

Bien que moisn recommandé, il est également possible de se connecter avec son compte GitHub ou Google. Le nom d'utilisateur sera à communiquer à Nicolas Godet ou Nicolas Van Hecke afin que vous soyez ajouté au groupe ISL.